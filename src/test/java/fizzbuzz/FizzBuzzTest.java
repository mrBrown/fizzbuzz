package fizzbuzz;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.*;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FizzBuzzTest {

    @ParameterizedTest(name = "Should count up to {0}")
    @CsvSource( {
            "0",
            "1, 1",
            "2, 1,2",
            "3, 1,2,Fizz",
            "4, 1,2,Fizz,4",
            "5, 1,2,Fizz,4,Buzz",
            "6, 1,2,Fizz,4,Buzz,Fizz",
            "10, 1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz",
    })
    void shouldCountCorrectly(int upTo, @AggregateWith(ArrayAggregator.class) String[] expected) {
        List<String> result = FizzBuzz.generate(upTo);

        assertEquals(List.of(expected), result);
    }

    @Test
    void shouldReturnCorrectResultFor15() {
        String result = FizzBuzz.generate(15).get(14);

        assertEquals("FizzBuzz", result);
    }

    static class ArrayAggregator implements ArgumentsAggregator {

        @Override
        public Object aggregateArguments(ArgumentsAccessor arguments, ParameterContext context) {
            String[] result = new String[arguments.size() - 1];

            for (int i = 0; i < result.length; i++) {
                result[i] = arguments.getString(i + 1);
            }

            return result;
        }

    }

}
