package fizzbuzz;


import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

    public static List<String> generate(int upTo) {
        List<String> result = new ArrayList<>();
        for (int i = 1; i <= upTo; i++) {
            if (isDivisibleBy(i, 15)) {
                result.add("FizzBuzz");
            } else if (isDivisibleBy(i, 3)) {
                result.add("Fizz");
            } else if (isDivisibleBy(i, 5)) {
                result.add("Buzz");
            } else {
                result.add(String.valueOf(i));
            }
        }
        return result;
    }

    private static boolean isDivisibleBy(final int value, final int factor) {
        return value % factor == 0;
    }

}
